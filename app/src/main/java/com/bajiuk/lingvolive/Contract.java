package com.bajiuk.lingvolive;

public interface Contract {
    int MSG_REGISTER_CALLBACK = 1;
    int MSG_REMOVE_CALLBACK = 2;
    int MSG_REQUEST_REFRESH = 3;
    int MSG_ADD_POST = 4;
    int MSG_ERROR = 5;
    int MSG_REFRESH_FINISHED = 6;
}
