package com.bajiuk.lingvolive.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bajiuk.lingvolive.R;
import com.bajiuk.lingvolive.data.Free;
import com.bajiuk.lingvolive.data.Post;
import com.bajiuk.lingvolive.data.TranslationRequest;
import com.bajiuk.lingvolive.data.UserTranslation;

import java.util.ArrayList;
import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private List<Post> mPosts = new ArrayList<>();

    public void addPost(Post post) {
        mPosts.add(0, post);
        notifyItemInserted(0);
    }

    public void removeAll() {
        notifyItemRangeRemoved(0, mPosts.size());
        mPosts.clear();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_post, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = mPosts.get(position);
        if (post instanceof Free) {
            constructFree(holder, (Free) post);
        } else if (post instanceof TranslationRequest) {
            constructTranslationRequest(holder, (TranslationRequest) post);
        } else if (post instanceof UserTranslation) {
            constructUserTranslation(holder, (UserTranslation) post);
        }
        holder.mAuthor.setText(post.getAuthorName());
        holder.mId.setText("#" + post.getId());
    }

    private void constructFree(ViewHolder holder, Free post) {
        holder.mBody.setVisibility(View.VISIBLE);
        holder.mHeading.setVisibility(View.GONE);
        holder.mBody.setText(post.getMessage());
    }

    private void constructTranslationRequest(ViewHolder holder, TranslationRequest post) {
        holder.mBody.setVisibility(View.GONE);
        holder.mHeading.setVisibility(View.VISIBLE);
        holder.mHeading.setText(post.getHeading());
    }

    private void constructUserTranslation(ViewHolder holder, UserTranslation post) {
        holder.mBody.setVisibility(View.VISIBLE);
        holder.mHeading.setVisibility(View.VISIBLE);
        holder.mHeading.setText(post.getHeading());
        holder.mBody.setText(post.getTranslation());
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mAuthor;
        private TextView mHeading;
        private TextView mBody;
        private TextView mId;

        public ViewHolder(View view) {
            super(view);
            mAuthor = (TextView) view.findViewById(R.id.post_author);
            mHeading = (TextView) view.findViewById(R.id.post_heading);
            mBody = (TextView) view.findViewById(R.id.post_body);
            mId = (TextView) view.findViewById(R.id.post_id);
        }
    }
}
