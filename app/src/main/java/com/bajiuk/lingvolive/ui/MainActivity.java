package com.bajiuk.lingvolive.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.bajiuk.lingvolive.Contract;
import com.bajiuk.lingvolive.R;
import com.bajiuk.lingvolive.data.Post;
import com.bajiuk.lingvolive.service.FeedService;

public class MainActivity extends AppCompatActivity {
    private Messenger mMessenger;
    private Messenger mHandler = new Messenger(new IncomingHandler());
    private PostsAdapter mAdapter = new PostsAdapter();
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Contract.MSG_ADD_POST:
                    mAdapter.addPost((Post) msg.obj);
                    mLayoutManager.scrollToPosition(0);
                    break;
                case Contract.MSG_ERROR:
                    Toast.makeText(getApplicationContext(), (String) msg.obj, Toast.LENGTH_LONG).show();
                case Contract.MSG_REFRESH_FINISHED:
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRecycler();
        initSwipeRefresh();
        initService();
    }

    private void initService() {
        Intent intent = new Intent(this, FeedService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void initRecycler() {
        RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler);
        mLayoutManager = new LinearLayoutManager(this);
        recycler.setAdapter(mAdapter);
        recycler.setLayoutManager(mLayoutManager);
    }

    private void initSwipeRefresh() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    mMessenger.send(Message.obtain(null, Contract.MSG_REQUEST_REFRESH));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    protected void onDestroy() {
        unbindService(mServiceConnection);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (mMessenger != null) {
                mAdapter.removeAll();
                mMessenger.send(Message.obtain(null, Contract.MSG_REGISTER_CALLBACK, mHandler));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mMessenger != null) {
                mMessenger.send(Message.obtain(null, Contract.MSG_REMOVE_CALLBACK));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMessenger = new Messenger(service);
            try {
                mMessenger.send(Message.obtain(null, Contract.MSG_REGISTER_CALLBACK, mHandler));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mMessenger = null;
        }
    };
}
