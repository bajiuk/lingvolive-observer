package com.bajiuk.lingvolive.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.bajiuk.lingvolive.Contract;
import com.bajiuk.lingvolive.data.Post;
import com.bajiuk.lingvolive.data.PostManager;
import com.bajiuk.lingvolive.net.FeedDownloader;

import java.util.List;

public class FeedService extends Service {
    private static final int REFRESH_TIME_MILLS = 1 * 60 * 1000;
    private Messenger mCallback;
    private IncomingHandler mHandler = new IncomingHandler();
    private Messenger mMessenger = new Messenger(mHandler);
    private FeedDownloader mFeedDownloader = new FeedDownloader(mHandler);
    private PostManager mPostManager = new PostManager();
    private Thread mDownloaderThread;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Contract.MSG_REGISTER_CALLBACK:
                    onRegisterCallback((Messenger) msg.obj);
                    break;
                case Contract.MSG_REMOVE_CALLBACK:
                    mCallback = null;
                    break;
                case Contract.MSG_REQUEST_REFRESH:
                    onRequestRefresh();
                    break;
                case Contract.MSG_REFRESH_FINISHED:
                    onPostsReceived((List<Post>) msg.obj);
                    break;
                case Contract.MSG_ERROR:
                    onErrorReceived((String) msg.obj);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private void onErrorReceived(String message) {
        try {
            mCallback.send(Message.obtain(null, Contract.MSG_ERROR, message));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void onRegisterCallback(Messenger callback) {
        mCallback = callback;
        sendPosts(mPostManager.getPosts());
    }

    private void sendPosts(List<Post> posts) {
        try {
            for (Post post : posts) {
                mCallback.send(Message.obtain(null, Contract.MSG_ADD_POST, post));
            }
            mCallback.send(Message.obtain(null, Contract.MSG_REFRESH_FINISHED));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void onPostsReceived(List<Post> posts) {
        mPostManager.addPosts(posts);
        if (mCallback != null) {
            mPostManager.addPosts(posts);
            sendPosts(mPostManager.getPostsToAdd());
            mPostManager.clearNewPosts();
        }
        mPostManager.clearNewPosts();
    }

    private void onRequestRefresh() {
        if (!mDownloaderThread.isAlive()) {
            mDownloaderThread = new Thread(mFeedDownloader);
            mDownloaderThread.start();
        }
    }

    @Override
    public void onCreate() {
        mDownloaderThread = new Thread(mFeedDownloader);
        mDownloaderThread.start();
        mHandler.post(mRefreshCode);
    }

    private Runnable mRefreshCode = new Runnable() {
        @Override
        public void run() {
            Message.obtain(mHandler, Contract.MSG_REQUEST_REFRESH).sendToTarget();
            mHandler.postDelayed(mRefreshCode, REFRESH_TIME_MILLS);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }
}
