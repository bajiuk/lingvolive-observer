package com.bajiuk.lingvolive;

import android.app.Application;
import android.content.Intent;

import com.bajiuk.lingvolive.service.FeedService;

public class ApplicationEx extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, FeedService.class));
    }
}
