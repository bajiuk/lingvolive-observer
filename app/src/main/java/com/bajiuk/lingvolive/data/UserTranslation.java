package com.bajiuk.lingvolive.data;

public class UserTranslation extends Post {

    private Type mType = Type.USER_TRANSLATION;
    private String mTranslation;
    private String mHeading;

    public UserTranslation(String authorName, int id, String heading, String translation) {
        super(authorName, id);
        mTranslation = translation;
        mHeading = heading;
    }

    public String getTranslation() {
        return mTranslation;
    }

    public String getHeading() {
        return mHeading;
    }
}
