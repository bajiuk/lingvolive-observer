package com.bajiuk.lingvolive.data;

import java.util.ArrayList;
import java.util.List;

public class PostManager {

    private List<Post> mPosts = new ArrayList<>();
    private List<Post> mPostsToAdd = new ArrayList<>();

    public void addPosts(List<Post> newPosts) {
        for (int i = newPosts.size() - 1; i >= 0; --i) {
            if (!isOldPost(newPosts.get(i))) {
                mPostsToAdd.add(newPosts.get(i));
                mPosts.add(newPosts.get(i));
            }
        }
    }

    public void clearNewPosts() {
        mPostsToAdd.clear();
    }

    private boolean isOldPost(Post post) {
        for (Post old : mPosts) {
            if (post.equals(old)) {
                return true;
            }
        }
        return false;
    }

    public List<Post> getPostsToAdd() {
        return mPostsToAdd;
    }

    public List<Post> getPosts() {
        return mPosts;
    }
}
