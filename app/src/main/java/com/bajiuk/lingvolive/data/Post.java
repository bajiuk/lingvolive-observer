package com.bajiuk.lingvolive.data;

import android.util.JsonReader;

import java.io.IOException;

public class Post {

    public enum Type {
        TRANSLATION_REQUEST,
        USER_TRANSLATION,
        FREE
    }

    public Post(String authorName, int id) {
        mAuthorName = authorName;
        mId = id;
    }

    private Type mType;
    private String mAuthorName;
    private int mId;


    public Type getType() {
        return mType;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public int getId() {
        return mId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Post) {
            return mId == ((Post) o).mId;
        } else {
            return super.equals(o);
        }
    }

    public static Post fromJson(JsonReader reader) throws IOException {
        String type = "";
        String author = "";
        String message = "";
        String translation = "";
        String heading = "";
        int id = 0;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("message")) {
                message = reader.nextString();
            } else if (name.equals("postDbId")) {
                id = reader.nextInt();
            } else if (name.equals("author")) {
                author = getAuthor(reader);
            } else if (name.equals("postType")) {
                type = reader.nextString();
            } else if (name.equals("translation")) {
                translation = reader.nextString();
            } else if (name.equals("heading")) {
                heading = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        if (type.equals("UserTranslation")) {
            return new UserTranslation(author, id, heading, translation);
        } else if (type.equals("TranslationRequest")) {
            return new TranslationRequest(author, id, heading);
        } else {
            return new Free(author, id, message);
        }
    }

    private static String getAuthor(JsonReader reader) throws IOException {
        String result = "";
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                result = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return result;
    }
}
