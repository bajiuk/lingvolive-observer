package com.bajiuk.lingvolive.data;

public class TranslationRequest extends Post {

    private Type mType = Type.TRANSLATION_REQUEST;
    private String mHeading;

    public TranslationRequest(String authorName, int id, String heading) {
        super(authorName, id);
        mHeading = heading;
    }

    public String getHeading() {
        return mHeading;
    }
}
