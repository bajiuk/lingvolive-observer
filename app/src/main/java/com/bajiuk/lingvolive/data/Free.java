package com.bajiuk.lingvolive.data;

public class Free extends Post {

    private Type mType = Type.FREE;
    private String mMessage;

    public Free(String authorName, int id, String message) {
        super(authorName, id);
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}
