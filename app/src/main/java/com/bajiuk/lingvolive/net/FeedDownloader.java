package com.bajiuk.lingvolive.net;

import android.os.Handler;
import android.os.Message;
import android.util.JsonReader;
import android.util.JsonToken;

import com.bajiuk.lingvolive.Contract;
import com.bajiuk.lingvolive.data.Post;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FeedDownloader implements Runnable {
    private static final String FEED_URL = "http://lingvolive.ru/api/social/feed/page?pageSize=10";
    private Handler mCallback;

    public FeedDownloader(Handler messenger) {
        mCallback = messenger;
    }

    @Override
    public void run() {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(FEED_URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            List<Post> posts = getPostsFromStream(urlConnection.getInputStream());
            sendPosts(posts);
        } catch (MalformedURLException e) {
            Message.obtain(mCallback, Contract.MSG_ERROR, "Invalid url! " + e.getMessage()).sendToTarget();
        } catch (IOException e) {
            Message.obtain(mCallback, Contract.MSG_ERROR, "Network error! " + e.getMessage()).sendToTarget();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    private List<Post> getPostsFromStream(InputStream in) {
        List result = new ArrayList();
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("posts") && reader.peek() != JsonToken.NULL) {
                    result = readPostsArray(reader);
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
            Message.obtain(mCallback, Contract.MSG_ERROR, "JsonReading error! " + e.getMessage()).sendToTarget();
        }
        return result;
    }

    private List<Post> readPostsArray(JsonReader reader) throws IOException {
        List<Post> posts = new ArrayList();
        reader.beginArray();
        while (reader.hasNext()) {
            posts.add(Post.fromJson(reader));
        }
        reader.endArray();
        return posts;
    }

    private void sendPosts(List<Post> posts) {
        Message.obtain(mCallback, Contract.MSG_REFRESH_FINISHED, posts).sendToTarget();
    }

}
